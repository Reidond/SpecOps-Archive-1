# Список изменений

Все заметные изменения в этом проекте будут задокументированы в этом файле.

Формат основан на [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
и этот проект придерживается [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Текущий список модификаций:

1. [CUP Terrains - Core](https://steamcommunity.com/workshop/filedetails/?id=583496184)
2. [CUP Terrains - Maps](https://steamcommunity.com/sharedfiles/filedetails/?id=583544987)
3. [RHSAFRF](https://steamcommunity.com/sharedfiles/filedetails/?id=843425103)
4. [RHSUSAF](https://steamcommunity.com/sharedfiles/filedetails/?id=843577117)
5. [RHSGREF](https://steamcommunity.com/sharedfiles/filedetails/?id=843593391)
6. [RHSSAF](https://steamcommunity.com/sharedfiles/filedetails/?id=843632231)
7. [YuEmod](https://steamcommunity.com/sharedfiles/filedetails/?id=1195262135)
8. [Spec Ops](https://gitlab.com/Reidond/SpecOps)
9. [Islands](https://gitlab.com/Reidond/Islands)


## [3.7 Patch 1] - 2019-05-20

### Добавлено

* [KAM - RHS Compatibility](https://steamcommunity.com/sharedfiles/filedetails/?id=1689001925)
* [Russian Helmets Project](https://steamcommunity.com/sharedfiles/filedetails/?id=1663652530)
* [PanterSOF Gear](http://tf373.us/373repo/@tf373pack/addons/)

### Обновлено

* [Community Base addons A3](https://steamcommunity.com/sharedfiles/filedetails/?id=450814997&searchtext=cba)
* [3den Enhanced](https://steamcommunity.com/sharedfiles/filedetails/?id=623475643)
* [Grenade Window Breaker](https://steamcommunity.com/sharedfiles/filedetails/?id=1702704179)
* [JSHK Contamination Area Mod](https://steamcommunity.com/sharedfiles/filedetails/?id=944344838)

### Исправлено

* [#192](https://gitlab.com/Reidond/SpecOps/issues/192)
* [#191](https://gitlab.com/Reidond/SpecOps/issues/191)
* [#186](https://gitlab.com/Reidond/SpecOps/issues/186)
* [#185](https://gitlab.com/Reidond/SpecOps/issues/185)
* [#184](https://gitlab.com/Reidond/SpecOps/issues/184)
* [#182](https://gitlab.com/Reidond/SpecOps/issues/182)

### Удалено

* [VSM Gorka-3](https://drive.google.com/drive/folders/1xayTTMacIiMfSCdmqojaVjr2kqM3JVA-) Аналоги есть в YuEmod

## [3.7] - 2019-05-03

### Добавлено

* [Jason's GPNVG-18 Night Vision](https://steamcommunity.com/sharedfiles/filedetails/?id=1714019280)
* [VET_Unflipping](https://steamcommunity.com/sharedfiles/filedetails/?id=1703187116)
* [dzn Rifle Tripod](https://steamcommunity.com/sharedfiles/filedetails/?id=1720595785)
* PVS-31

### Обновлено

* [NIArms Core - RHS Compatibility](https://steamcommunity.com/sharedfiles/filedetails/?id=1541041923)
* [NIArms Core](https://steamcommunity.com/sharedfiles/filedetails/?id=667454606)
* [NIArms P226 Pistols](https://steamcommunity.com/sharedfiles/filedetails/?id=1091513876)
* [NIArms G3 Rifles](https://steamcommunity.com/sharedfiles/filedetails/?id=667375637)
* [NIArms MP5 SMGs](https://steamcommunity.com/sharedfiles/filedetails/?id=1541041923)
* [MRH Satellite](https://steamcommunity.com/sharedfiles/filedetails/?id=1310581330)
* [3den Enhanced](https://steamcommunity.com/sharedfiles/filedetails/?id=623475643)
* [Grenade Window Breaker](https://steamcommunity.com/sharedfiles/filedetails/?id=1702704179)
* [dzn Vehicle on Fire](https://steamcommunity.com/sharedfiles/filedetails/?id=1352522482)
* [Community Base addons A3](https://steamcommunity.com/sharedfiles/filedetails/?id=450814997&searchtext=cba)

### Исправлено

* [#181](https://gitlab.com/Reidond/SpecOps/issues/181)
* [#178](https://gitlab.com/Reidond/SpecOps/issues/178)
* [#176](https://gitlab.com/Reidond/SpecOps/issues/176)
* [#174](https://gitlab.com/Reidond/SpecOps/issues/174)
* [#173](https://gitlab.com/Reidond/SpecOps/issues/173)
* [#172](https://gitlab.com/Reidond/SpecOps/issues/172)
* [#169](https://gitlab.com/Reidond/SpecOps/issues/169)
* [#166](https://gitlab.com/Reidond/SpecOps/issues/166)
* [#165](https://gitlab.com/Reidond/SpecOps/issues/165)
* [#164](https://gitlab.com/Reidond/SpecOps/issues/164)
* [#163](https://gitlab.com/Reidond/SpecOps/issues/163)
* [#162](https://gitlab.com/Reidond/SpecOps/issues/162)
* [#160](https://gitlab.com/Reidond/SpecOps/issues/160)
* [#159](https://gitlab.com/Reidond/SpecOps/issues/159)
* [#158](https://gitlab.com/Reidond/SpecOps/issues/158)
* [#157](https://gitlab.com/Reidond/SpecOps/issues/157)
* [#155](https://gitlab.com/Reidond/SpecOps/issues/155)
* [#154](https://gitlab.com/Reidond/SpecOps/issues/154)
* [#153](https://gitlab.com/Reidond/SpecOps/issues/153)
* [#149](https://gitlab.com/Reidond/SpecOps/issues/149)
* [#147](https://gitlab.com/Reidond/SpecOps/issues/147)
* [#146](https://gitlab.com/Reidond/SpecOps/issues/146)
* [#136](https://gitlab.com/Reidond/SpecOps/issues/136)

### Удалено

* ST11 GPNVG-18 Заменен новым модом
* PATHuK Gear Невостребован 
* [BCA](https://steamcommunity.com/sharedfiles/filedetails/?id=1087021903) Невостребован
* [RH Pistol pack](http://www.armaholic.com/page.php?id=20881) Невостребован
* [Breaching Charge](https://steamcommunity.com/sharedfiles/filedetails/1314910827) Невостребован

## [3.6 Patch 3] - 2019-04-02

### Обновлено

* [Community Base addons A3](https://steamcommunity.com/sharedfiles/filedetails/?id=450814997)
* [KAT - Advanced Medical](https://steamcommunity.com/sharedfiles/filedetails/?id=1626728054)

### Удалено

* [NIArms AWM Rifles](https://steamcommunity.com/sharedfiles/filedetails/?id=509730197) Невостребован
* [Don't remove empty mags](http://www.armaholic.com/page.php?id=27697) Баги
* [RKSL Attachments Pack](https://steamcommunity.com/sharedfiles/filedetails/?id=1661066023) Невостребован
* [Enhanced Soundscape](https://steamcommunity.com/sharedfiles/filedetails/?id=825179978) Фризы/лаги
* [Sweet markers system](http://www.armaholic.com/page.php?id=27012) В пользу лучшего FPS заменен на ACE Markers 
* [NIArms G36 Rifles](https://steamcommunity.com/sharedfiles/filedetails/?id=594378620) Невостребован
* [NIArms SG550 Rifles](https://steamcommunity.com/sharedfiles/filedetails/?id=1399779368) Невостребован
* [@maritime_helmetV2](https://steamcommunity.com/sharedfiles/filedetails/?id=1473631398) В ожидании Maritime'ов из мода UnderSiege Gear & Uniforms
* [NAPA reArmed](https://steamcommunity.com/sharedfiles/filedetails/?id=1507820054) Невостребован

## [3.6 Patch 2] - 2019-03-15

### Обновлено

* [3den Enhanced](https://steamcommunity.com/sharedfiles/filedetails/?id=623475643)
* [GRAD Trenches](https://steamcommunity.com/sharedfiles/filedetails/?id=1224892496)
* [Tactical Weapon Swap](http://www.armaholic.com/page.php?id=33785)   
* [Community Base addons A3](https://steamcommunity.com/sharedfiles/filedetails/?id=450814997)
* [Advanced Combat Environment 3 (ACE 3)](http://www.armaholic.com/page.php?id=28557)
* [ACEX](http://www.armaholic.com/page.php?id=31252)

## [3.6 Patch 1] - 2019-03-09

### Обновлено

* [Enhanced Movement (ACEX Fix)](https://steamcommunity.com/sharedfiles/filedetails/?id=1586691629)
* [Tactical Position Ready](http://www.armaholic.com/page.php?id=33953) 
* [Tactical Weapon Swap](http://www.armaholic.com/page.php?id=33785)   
* [KAT - Advanced Medical](https://steamcommunity.com/sharedfiles/filedetails/?id=1626728054)

### Удалено

* [NIArms MG3 GPMGs](https://steamcommunity.com/sharedfiles/filedetails/?id=774809509)

## [3.6] - 2019-03-03

### Добавлено

* [KAT - Advanced Medical](https://steamcommunity.com/sharedfiles/filedetails/?id=1626728054)

### Обновлено

* [Window Breaker](https://steamcommunity.com/sharedfiles/filedetails/?id=1578884800)
* [Enhanced Movement](https://steamcommunity.com/sharedfiles/filedetails/?id=333310405)
* [GRAD Trenches](https://steamcommunity.com/sharedfiles/filedetails/?id=1224892496)
* [ADV - ACE Medical](https://steamcommunity.com/sharedfiles/filedetails/?id=1353873848)
* [MS IFF Strobe](https://steamcommunity.com/sharedfiles/filedetails/?id=1543390130)
* [RKSL Studios: Attachments](https://steamcommunity.com/sharedfiles/filedetails/?id=1661066023)
* [3den Enhanced](https://steamcommunity.com/sharedfiles/filedetails/?id=623475643)
* [KAT - Advanced Medical](https://steamcommunity.com/sharedfiles/filedetails/?id=1626728054)

## [3.5 Patch 1] - 2019-01-28

### Изменено

* Настройки ACE:
  * Все параметры "ACE Scopes" теперь принудительны для всех.
  * ace_advanced_ballistics_simulationInterval с 0.05 до 0.10 (Возможен прирост фпс, ТЕСТ)
  * ace_winddeflection_simulationInterval с 0.05 до 0.10 (Возможен прирост фпс, ТЕСТ)
  * ace_medical_enableUnconsciousnessAI с disabled на 50/50
  * ace_hearingunconsciousnessVolume с 0.4 на 0.8
* У такистанцев:
  * Убраны все декали у техники (кроме АН-2): номерные знаки, номерные обозначения
  * Добавлен новый боец: Crewman
  * В БТР-60ПБ и БМП-1 теперь сидят Crewman
  * У АН-2 заменена раскраска на AirTak, а также номер на хвост теперь красный

## [3.5] - 2019-01-26

### Добавлено

* Новые vests из мода P-TAC
* Ли-Энфилды из данного мода: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=807038742&searchtext=infinite)
* Новые рюкзаки из мода [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1292963000&searchtext=tholozor)
* Недостающие текстуры для Eotech 552 из мода SMA
* Добавлены новые фракции: Takistani Locals и Civilians (Takistan)
* Добавлено панамки, шапочки и кепочки из мода ST11

### Изменено

* Настройки ACE, а именно:
  * ace_captives_requireSurrender теперь "Откл."
  * в пункте ace_captives_allowHandcuffOwnside теперь стоит галочка
  * ace_nightvision_fogScaling с 0.8 до 0.4
  * "Макс. количество отслеживаемых снарядов" с 10 до 7
  * "Макс. количество снарядов за кадром" с 10 до 7
  * в пункте ace_pylons_requireEngineer теперь стоит галочка
  * ace_cookoff_ammoCookoffDuration с 1.0 до 0.7
* Изменены текстурки у панамок, шапочек и кепочек из мода ST11

### Обновлено

* Текстуры мультикама у рюкзаков LBT 2595
* Tactical Position Ready [Armaholic](http://www.armaholic.com/page.php?id=33953)

### Удалено

* Helicopter Dust Efx Mod, конкретно: ANZ_HeliDustEfxMod.pbo
* Модуль "Прятки" за ненадобности\не работоспособности
* Следующие головные уборы из мода VSM/Project Zenith
  * VSM_HAT_Multicam
  * VSM_HAT_OD
  * VSM_HAT_TAN

## [3.3.1] - 2019-01-07

### Добавлено

* Такистанская армия (ТЕСТ), подробнее: [Armaholic](http://www.armaholic.com/page.php?id=34761)
* Новые версии PCU+G3

### Обновлено

* CBA, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=18767)
* ADV - ACE Medical, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1353873848&searchtext=ACE+Medical)
* 3den Enhanced, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=30461)
* Модули ACE: Medical и Nightvision
* Зимняя униформа
* Названия и нашивки рюкзаков LBT
* ACE 3 Extension, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=766491311)
* CPC жилеты из P-TAC, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1560229549&searchtext=P-TAC)

### Удалено

* Splendid Smoke - сильно понижал FPS во время использования дымов.
* Walkable Moving Objects - ломал приземление с парашутом, позволял забираться на любые объекты и пр.

## [3.3] - 2019-01-01

### Добавлено

* Добавлены жилеты JPC в камуфляжах AOR1 и AOR2

### Обновлено

* Текстуры жилетов MarCiras
* P-TAC униформа, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1560229549&searchtext=P-TAC)

### Удалено

* Шлемы Tholozor's SOF - невостребованы

## [3.2] - 2018-12-23

### Добавлено

* P-TAC Utility Files, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1560229549)
* Assault Boat Patch, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1603728666)
* BCA, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1087021903&searchtext=bca)
* Различные рюкзаки из мода @JSOC
* Различные жилеты из мода @JSOC
* Breaching Charge, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1314910827)
* @maritime_helmetV2, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1473631398)
* Жилеты ССО из мода @ST11
* Шлемы MICH 2000 из мода @ST11
* Жилеты LBT из мода @ST11
* Различные аксессуары из мода @ST11
* Версия GPNVG-18 под MICH из мода @ST11
* Helicopter Dust Efx Mod, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1537745369)
* AN/PVS-21 Low Profile NVG, подробнее: [Bohemia Interactive Forums](https://forums.bohemia.net/forums/topic/196145-anpvs-21-wip/)

### Обновлено

* 3den Enhanced, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=30461)
* CHDKZ reArmed, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1216707909)
* GRAD Trenches, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33555)
* NAPA reArmed, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1507820054)
* Логотип Spec Ops
* Шлемы Fast Ballistic в Military Gear Pack

### Удалено

* Балаклавы Oakley SI Halo Goggles - невостребованы
* Щит "Вант-ВМ" - невостребован
* Некоторые пистолеты из RH Pistol pack - невостребованны
* Винтовка Lee Enfield - невостребована

## [3.1] - 2018-12-08

### Добавлено

* NIArms SG550 Rifles, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1399779368&searchtext=sig)
* NIArms MG3 GPMGs, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=774809509)
* NIArms P226 Pistols, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1091513876)
* Различная экипировка: жилеты, балаклавы, шемаги, банданы и т.д (в том числе из Ghost Recon Wildlands)

### Удалено

* Униформа G2 - невостребованна

## [3.0] - 2018-12-05

### Добавлено

* Window Breaker, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1578884800)

### Обновлено

* ACE до версии 3.12.5, подробнее: [Steam Workshop](https://steamcommunity.com/workshop/filedetails/?id=463939057)
* ACE Compat - RHS: GREF до версии 3.12.5, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=884966711)
* ACE Compat - RHS Armed Forces of the Russian Federation до версии 3.12.5, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=773131200)
* ACE Compat - RHS United States Armed Forces до версии 3.12.5, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=773125288)
* ACEX до версии 3.4.1, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=708250744)

### Удалено

* Урал-Федерал и "буханка" из СПР Мода - невостребованны

## [2.9.9] - 2018-11-19

### Добавлено

* Oakley SI Halo Goggles, подробнее:  [Armaholic](http://www.armaholic.com/page.php?id=34403)
* (возвращены) бинокли и пнв из VTN
* HK471 из SPS-Weapons, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1069692316&searchtext=hk417)
* Некоторые жилеты из Aspis Gear (Retextures), подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=900775311&searchtext=aspis+gear)

### Обновлено

* ASR AI3 - RHS config до версии 0.4.8.1, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33116)
* ASR AI 3 до версии 1.1.9, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=24080)
* GRAD Trenches до версии 1.5.2, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33555)
* CBA, подробнее: [Github](https://github.com/CBATeam/CBA_A3/commits/master)
* ACE до версии 3.12.4, подробнее: [Steam Workshop](https://steamcommunity.com/workshop/filedetails/?id=463939057)
* ACE Compat - RHS: GREF до версии 3.12.4, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=884966711)
* ACE Compat - RHS Armed Forces of the Russian Federation до версии 3.12.4, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=773131200)
* ACE Compat - RHS United States Armed Forces до версии 3.12.4, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=773125288)
* ACEX до версии 3.4.0, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=708250744)
* speclib -> 2.9.9, подробнее: [Gitlab](https://gitlab.com/Reidond/SpecOps_Dev/)
* Suppress до версии 2.04, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=825174634)
* Sweet markers system
* C.O.S., подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=846603147&searchtext=cos)

### Удалено

* Advanced Urban Rappelling - баги, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=730310357&searchtext=)
* OTK ChestRigs Retextures Pack, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1180280065) - невостребован


## [2.9.8] - 2018-11-04

### Обновлено

* ACE3, подробнее: [Github](https://github.com/acemod/ACE3/commits/master)
* ACEX, подробнее: [Github](https://github.com/acemod/ACEX/commits/master)
* CBA, подробнее: [Github](https://github.com/CBATeam/CBA_A3/commits/master)

### Изменено

* speclib_arsenal_optimization, испльзование макросов для удобства скрывания ненужных вещей, подробнее: [Gitlab](https://gitlab.com/Reidond/SpecOps_Dev/commit/97bb25e27bbe4fbfbee690a885f2644043517d01)
* speclib_protection_settings, в следствии реорганизации структуры модов были забыты наушники, подробнее: [Gitlab](https://gitlab.com/Reidond/SpecOps/commit/e20fb6bbdb66273425d63c55f346f2c4945fc83d) и [Gitlab](https://gitlab.com/Reidond/SpecOps_Dev/commit/97bb25e27bbe4fbfbee690a885f2644043517d01)

## [2.9.7] - 2018-10-28

### Добавлено

* Сборники NiArms

### Обновлено

* NIArms AWM Rifles до версии 1.51, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=28950)
* NIArms Core до версии 1.55, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=24620)
* NIArms Core - RHS Compatibility, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1541041923)

### Удалено

* Weapon Eventhandler Framework: [Armaholic](http://www.armaholic.com/page.php?id=28954) (невостребован)

## [2.9.6] - 2018-10-12

### Обновлено

* ADV - ACE Medical до версии 1.0.4, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1353873848&searchtext=ACE+Medical)
* Community Base addons A3 до версии 3.9.0.181012, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=18767)
* WMO - Walkable Moving Objects до версии 0.9, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=925018569)
* GRAD Trenches до версии 1.5.1, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33555)

## [2.9.5] - 2018-09-23

### Добавлено

* Project Zenith, конкретно: VSM_ColdWeatherGear, VSM_G2, VSM_g3, VSM_Gloves, VSM_Zenith_Headgear, VSM_Zenith_Vests
* NAPA reArmed, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1507820054)
* (перенесен из [@Islands](https://gitlab.com/Reidond/Islands)) ZECCUP - композиций CUP обектов, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=750186990)
* (перенесен из [@Islands](https://gitlab.com/Reidond/Islands)) ZEC - композиций обектов, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=642912021)
* speclib_snow_storm - модуль снежной бури, подробнее [Armaholic](http://www.armaholic.com/page.php?id=31910) и [SpecOps_Dev](https://gitlab.com/Reidond/SpecOps_Dev/tree/master/addons/snow_storm)

### Обновлено

* 3den Enhanced до версии 3.4, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=30461)
* CHDKZ reArmed до версии 1.2, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1216707909)
* speclib_* до версии 2.9.5 cream, подробнее [Armaholic](https://gitlab.com/Reidond/SpecOps_Dev)

## [2.9.4] - 2018-08-28

### Добавлено

* Мод CHDKZ reArmed, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1216707909)
* Комплекс ОЦ-14-4 "Гроза" из мода СПР: Специальные подразделения России
* Техника УАЗ-451, Урал-42591 «Федерал») из мода СПР: Специальные подразделения России
* Винтовка Lee Enfield для моджахедов
* Бинокль М22 из VETERAN MOD
* РЛС Фара-ПВ из VETERAN MOD
* Радиостанции ДВ диапазона: AN/PRC-117F, AN/PRC-150, Р-168-5КНЕ из VETERAN MOD
* Крупнокалиберный пулемёт "Корд" с СПП
* Улучшение шрифтов Sweet markers system
* ACE Interaction Menu Expansion, конкретно group.pbo и shared.pbo, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33987)
* Лодка Duarry Supercat из мода FFAA, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=820994401&searchtext=ffaa)
* Crye G3 Navy Custom (Project Zenith Retexture), подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1413761939&searchtext=)
* Don't remove empty mags, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=27697)
* WMO - Walkable Moving Objects, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=925018569)
* Splendid Smoke, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=770418577)
* dzn Weapon Holders Carryable, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1418108437)
* C.O.S., подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=846603147&searchtext=C.O.S)
* JSHK Contamination Area Mod, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=944344838) (замена старого скрипта для противогазов)
* MGM Grips, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1386093495)
* Вант-ВМ, АПС, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1369953203)
* PVS-27 Interact, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=32794)
* MRH Satellite, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1310581330)
* dzn Vehicle on Fire, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33877)
* Zombies and Demons, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=28958)

### Обновлено

* GS ARMA 3 Weapons pack от 24 августа, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1218148105)
* dzn Artillery Illumination от 18 августа, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1396901301)
* dzn Extended Jamming от 25 августа, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1379304937)
* Advanced Weapon Mounting до версии 1.0.4, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1378046829)
* Diwako's ACE Ragdolling  до версии 1.2.1, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1435263885)
* Diwako's STALKER-like anomalies от 10го августа, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1383903166)
* ADV - ACE Medical до версии 1.0.3, подробнее: [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1353873848&searchtext=ACE+Medical)
* ACE3 до версии 3.12.3, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=28557)
* ACEX до версии 3.3.1, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=31252)
* Tactical Position Ready  до версии 1.3.4, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33953)
* Tactical Weapon Swap  до версии 0.1.9.2, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33785)
* Community Base addons A3 до версии 3.8.0.180801, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=18767)
* 3den Enhanced до версии 3.3, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=30461)
* Enhanced Movement до версии 0.8.4.15, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=27224)
* GRAD Trenches до версии 1.4.91, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=33555)
* Enhanced Soundscape до версии 1.23, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=26780)
* ASR AI 3 до версии 1.1.7 + конфиги к нему, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=24080)
* NIArms Core до версии 1.4, подробнее: [Armaholic](http://www.armaholic.com/page.php?id=24620)

### Удалено

* Портированый БРДМ-2А - морально устарел
* Сборник формы G3 от Cunico (заменен на более качетсвенный аналог)
* GPNVG-18 - текстурная ошибка, отсутсвие контура во время использования
* LAxemann's "Mount" (заменен Advanced Weapon Mounting)
* Химкостюм (ошибка конфига)
* speclib_difficulty_main.pbo (ошибка конфига)
* Кепки MilGearPack'a  (морально устарели)
* ЗИЛ-131 (присутсвует аналог в RHS)
* Сборник MG3 (присутсвует аналог в RHS)
* GSTAVO's M16A2  (отсутствие динамических магазинов)
* Сборник FN FAL (в ожиданий RHS'овских)
